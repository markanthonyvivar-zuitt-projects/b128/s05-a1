USE classic_db;

SELECT customerName FROM customers WHERE country = "Philippines";

SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

SELECT customerName FROM customers WHERE state IS NULL;

SELECT firstName,lastName,email FROM employees WHERE firstName = "Steve" AND lastName = "Patterson";

SELECT customerName,country,creditLimit FROM customers WHERE country != "USA"
AND creditLimit > 3000;

SELECT customerName FROM customers WHERE customerName NOT LIKE "%a%";

SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%" OR comments LIKE "%DHL" OR comments LIKE "DHL%";

SELECT productLine FROM products WHERE productDescription LIKE "%state of the art%" OR productDescription LIKE "%state of the art" OR productDescription LIKE "state of the art%";

SELECT country FROM customers GROUP BY country;

SELECT status FROM orders GROUP BY status;

SELECT customerName,country FROM customers WHERE country = "USA" OR country = "France" OR country = "Canada";

SELECT firstName,lastName,city FROM employees,offices WHERE employees.officeCode = 5 AND offices.officeCode = 5;

SELECT customerName FROM customers WHERE contactFirstName = "Leslie" AND contactLastName = "Thompson";

SELECT firstName,lastName FROM employees WHERE reportsTo = 1056 AND firstName != "Anthony";

SELECT productName,MSRP FROM products WHERE MSRP = (SELECT MAX(MSRP) FROM products);

SELECT COUNT(*) FROM customers WHERE country = "UK";